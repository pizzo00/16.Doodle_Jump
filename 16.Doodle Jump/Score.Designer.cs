﻿namespace _16.Doodle_Jump
{
    partial class Score
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Score));
            this.btt_menu = new System.Windows.Forms.PictureBox();
            this.personaggio = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.lbl_punteggio1 = new System.Windows.Forms.Label();
            this.lbl_punteggio2 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lbl_punteggio3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.btt_menu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.personaggio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btt_menu
            // 
            this.btt_menu.BackColor = System.Drawing.Color.Transparent;
            this.btt_menu.Image = global::_16.Doodle_Jump.Properties.Resources.menu;
            this.btt_menu.Location = new System.Drawing.Point(226, 503);
            this.btt_menu.Name = "btt_menu";
            this.btt_menu.Size = new System.Drawing.Size(106, 40);
            this.btt_menu.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btt_menu.TabIndex = 13;
            this.btt_menu.TabStop = false;
            this.btt_menu.Click += new System.EventHandler(this.btt_menu_Click);
            // 
            // personaggio
            // 
            this.personaggio.BackColor = System.Drawing.Color.Transparent;
            this.personaggio.Image = global::_16.Doodle_Jump.Properties.Resources.carattereDX;
            this.personaggio.Location = new System.Drawing.Point(25, 499);
            this.personaggio.Name = "personaggio";
            this.personaggio.Size = new System.Drawing.Size(46, 44);
            this.personaggio.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.personaggio.TabIndex = 12;
            this.personaggio.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.Image = global::_16.Doodle_Jump.Properties.Resources.barretta;
            this.pictureBox2.Location = new System.Drawing.Point(18, 544);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(53, 15);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox2.TabIndex = 11;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = global::_16.Doodle_Jump.Properties.Resources.titolo;
            this.pictureBox1.Location = new System.Drawing.Point(12, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(266, 84);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 10;
            this.pictureBox1.TabStop = false;
            // 
            // timer
            // 
            this.timer.Enabled = true;
            this.timer.Interval = 10;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // lbl_punteggio1
            // 
            this.lbl_punteggio1.BackColor = System.Drawing.Color.Transparent;
            this.lbl_punteggio1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_punteggio1.Location = new System.Drawing.Point(195, 201);
            this.lbl_punteggio1.Name = "lbl_punteggio1";
            this.lbl_punteggio1.Size = new System.Drawing.Size(88, 18);
            this.lbl_punteggio1.TabIndex = 17;
            this.lbl_punteggio1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbl_punteggio2
            // 
            this.lbl_punteggio2.BackColor = System.Drawing.Color.Transparent;
            this.lbl_punteggio2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_punteggio2.Location = new System.Drawing.Point(195, 277);
            this.lbl_punteggio2.Name = "lbl_punteggio2";
            this.lbl_punteggio2.Size = new System.Drawing.Size(88, 18);
            this.lbl_punteggio2.TabIndex = 16;
            this.lbl_punteggio2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(41, 201);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(148, 18);
            this.label2.TabIndex = 15;
            this.label2.Text = "Punteggio Record:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(80, 277);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(109, 18);
            this.label1.TabIndex = 14;
            this.label1.Text = "2° Punteggio:";
            // 
            // lbl_punteggio3
            // 
            this.lbl_punteggio3.BackColor = System.Drawing.Color.Transparent;
            this.lbl_punteggio3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_punteggio3.Location = new System.Drawing.Point(195, 356);
            this.lbl_punteggio3.Name = "lbl_punteggio3";
            this.lbl_punteggio3.Size = new System.Drawing.Size(88, 18);
            this.lbl_punteggio3.TabIndex = 19;
            this.lbl_punteggio3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(80, 356);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(109, 18);
            this.label4.TabIndex = 18;
            this.label4.Text = "3° Punteggio:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Papyrus", 15.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(206, 63);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 33);
            this.label3.TabIndex = 20;
            this.label3.Text = "Scores";
            // 
            // Score
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::_16.Doodle_Jump.Properties.Resources.Sfondo;
            this.ClientSize = new System.Drawing.Size(344, 601);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lbl_punteggio3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lbl_punteggio1);
            this.Controls.Add(this.lbl_punteggio2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btt_menu);
            this.Controls.Add(this.personaggio);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Score";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Doodle Jump";
            ((System.ComponentModel.ISupportInitialize)(this.btt_menu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.personaggio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox btt_menu;
        private System.Windows.Forms.PictureBox personaggio;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.Label lbl_punteggio1;
        private System.Windows.Forms.Label lbl_punteggio2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbl_punteggio3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
    }
}