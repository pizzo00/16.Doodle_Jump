﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _16.Doodle_Jump
{
    public partial class StartScreen : Form
    {
        public StartScreen()
        {
            InitializeComponent();
        }

        int movimentoSuGiu = 10; // pos = su // neg = giu 

        private void btt_play_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Per muoverti orizzontalmente usa \"m\" ed \"n\"... ", "Istruzioni", MessageBoxButtons.OK, MessageBoxIcon.Information);
            Gioco formGioco = new Gioco();
            formGioco.Show();
        }

        private void timerMovimentoPersonaggio_Tick(object sender, EventArgs e)
        {
            int movimentoY;
            if (movimentoSuGiu > 0)
            {
                movimentoY = -2;
                movimentoSuGiu--;
            }
            else if (movimentoSuGiu < 0)
            {
                movimentoY = 2;
                movimentoSuGiu--;
            }
            else
            {
                movimentoY = 0;
                movimentoSuGiu--;
            }
            if (personaggio.Location.Y == 499)
            {
                movimentoSuGiu = 50;
            }
            personaggio.Location = new System.Drawing.Point(personaggio.Location.X, personaggio.Location.Y + movimentoY);
        }

        private void btt_score_Click(object sender, EventArgs e)
        {
            Score score = new Score();
            score.Show();
        }
    }
}
