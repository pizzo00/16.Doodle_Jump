﻿namespace _16.Doodle_Jump
{
    partial class StartScreen
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StartScreen));
            this.btt_play = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.personaggio = new System.Windows.Forms.PictureBox();
            this.timerMovimentoPersonaggio = new System.Windows.Forms.Timer(this.components);
            this.btt_score = new System.Windows.Forms.PictureBox();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.btt_play)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.personaggio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btt_score)).BeginInit();
            this.SuspendLayout();
            // 
            // btt_play
            // 
            this.btt_play.BackColor = System.Drawing.Color.Transparent;
            this.btt_play.Image = global::_16.Doodle_Jump.Properties.Resources.play;
            this.btt_play.Location = new System.Drawing.Point(67, 242);
            this.btt_play.Name = "btt_play";
            this.btt_play.Size = new System.Drawing.Size(106, 40);
            this.btt_play.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.btt_play.TabIndex = 0;
            this.btt_play.TabStop = false;
            this.btt_play.Click += new System.EventHandler(this.btt_play_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = global::_16.Doodle_Jump.Properties.Resources.titolo;
            this.pictureBox1.Location = new System.Drawing.Point(30, 30);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(266, 84);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.Image = global::_16.Doodle_Jump.Properties.Resources.barretta;
            this.pictureBox2.Location = new System.Drawing.Point(36, 544);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(53, 15);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox2.TabIndex = 2;
            this.pictureBox2.TabStop = false;
            // 
            // personaggio
            // 
            this.personaggio.BackColor = System.Drawing.Color.Transparent;
            this.personaggio.Image = global::_16.Doodle_Jump.Properties.Resources.carattereDX;
            this.personaggio.Location = new System.Drawing.Point(43, 499);
            this.personaggio.Name = "personaggio";
            this.personaggio.Size = new System.Drawing.Size(46, 44);
            this.personaggio.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.personaggio.TabIndex = 3;
            this.personaggio.TabStop = false;
            // 
            // timerMovimentoPersonaggio
            // 
            this.timerMovimentoPersonaggio.Enabled = true;
            this.timerMovimentoPersonaggio.Interval = 10;
            this.timerMovimentoPersonaggio.Tick += new System.EventHandler(this.timerMovimentoPersonaggio_Tick);
            // 
            // btt_score
            // 
            this.btt_score.BackColor = System.Drawing.Color.Transparent;
            this.btt_score.Image = global::_16.Doodle_Jump.Properties.Resources.scores;
            this.btt_score.Location = new System.Drawing.Point(190, 340);
            this.btt_score.Name = "btt_score";
            this.btt_score.Size = new System.Drawing.Size(106, 41);
            this.btt_score.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.btt_score.TabIndex = 4;
            this.btt_score.TabStop = false;
            this.btt_score.Click += new System.EventHandler(this.btt_score_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Papyrus", 15.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(167, 544);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(165, 33);
            this.label3.TabIndex = 21;
            this.label3.Text = "Davide Pizzolato";
            // 
            // StartScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::_16.Doodle_Jump.Properties.Resources.Sfondo;
            this.ClientSize = new System.Drawing.Size(344, 601);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btt_score);
            this.Controls.Add(this.personaggio);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btt_play);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "StartScreen";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Doodle Jump";
            ((System.ComponentModel.ISupportInitialize)(this.btt_play)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.personaggio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btt_score)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox btt_play;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox personaggio;
        private System.Windows.Forms.Timer timerMovimentoPersonaggio;
        private System.Windows.Forms.PictureBox btt_score;
        private System.Windows.Forms.Label label3;
    }
}

