﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;

namespace _16.Doodle_Jump
{
    public partial class GameOver : Form
    {
        public GameOver(string punteggio)
        {
            InitializeComponent();
            stampaPunteggi(punteggio);
        }

        int movimentoSuGiu = 10; // pos = su // neg = giu 
        const int RECORD      = 0;
        const int PUNTEGGIO_2 = 1;
        const int PUNTEGGIO_3 = 2;


        void stampaPunteggi(string punteggio)
        {
            string[] config = File.ReadAllLines(@"./config.txt");
            if (Convert.ToInt32(config[RECORD]) < Convert.ToInt32(punteggio))
            {
                config[PUNTEGGIO_3] = config[PUNTEGGIO_2];
                config[PUNTEGGIO_2] = config[RECORD];
                config[RECORD] = punteggio;
            }
            else if (Convert.ToInt32(config[PUNTEGGIO_2]) < Convert.ToInt32(punteggio))
            {
                config[PUNTEGGIO_3] = config[PUNTEGGIO_2];
                config[PUNTEGGIO_2] = punteggio;
            }
            else if (Convert.ToInt32(config[PUNTEGGIO_3]) < Convert.ToInt32(punteggio))
            {
                config[PUNTEGGIO_3] = punteggio;
            }
            File.WriteAllLines(@"./config.txt", config);
            lbl_punteggioRecord.Text = config[RECORD];
            lbl_punteggio.Text = punteggio;
        }

        private void animazioneJump_Tick(object sender, EventArgs e)
        {
            int movimentoY;
            if (movimentoSuGiu > 0)
            {
                movimentoY = -2;
                movimentoSuGiu--;
            }
            else if (movimentoSuGiu < 0)
            {
                movimentoY = 2;
                movimentoSuGiu--;
            }
            else
            {
                movimentoY = 0;
                movimentoSuGiu--;
            }
            if (personaggio.Location.Y == 499)
            {
                movimentoSuGiu = 50;
            }
            personaggio.Location = new System.Drawing.Point(personaggio.Location.X, personaggio.Location.Y + movimentoY);

        }

        private void btt_menu_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btt_playAgain_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Per muoverti orizzontalmente usa \"m\" ed \"n\"... ", "Istruzioni", MessageBoxButtons.OK, MessageBoxIcon.Information);
            Gioco gioco = new Gioco();
            gioco.Show();
            this.Close();
        }
    }
}
